import pickle

#pickle 불러오기
with open('News_summarize.pkl','rb') as f:
    data = pickle.load(f)
    print(data)

data['why'] = [[] for i in range(data.shape[0])]

# kkma tag
from konlpy.tag import *
kkma = Kkma()

def token(data):
    try:
        return kkma.pos(data)
    except:
        return None

#find Josa
def josa(token):
    try:
        idx = []
        for i, t in enumerate(token):
            if t[1] == 'VV':
                idx.append([t, i])
        return idx
    except:
        None

#why extraction
for i in range(len(data)):
    data_token = token(data['summarize_text'][i])
    data_josa = josa(data_token)
    for j in range(len(data_josa)):
        pre = data_josa[j-1]
        now = data_josa[j]
        word = []
        if now[0][0] == "위하":
            for k in range(pre[1], now[1]+2):
                word.append(data_token[k][0])
        if word:
            data['why'][i].append(''.join(word))
            
print('done')

#check
print(data)
data['why'][i] for i in range(10)] #10 result

#pickle로 저장
import pickle
with open('data_why.pkl','wb') as file:
	pickle.dump(data,file)

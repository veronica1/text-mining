import pickle

with open('News_summarize.pkl','rb') as f:
	data = pickle.load(f)

# where_ver2
data['where_ver2'] = [[] for i in range(data.shape[0])]

for i in range(len(data)):
    sentences = data['Clean_News'][i]
    for j, s in enumerate(sentences):
        words = s.split(" ")
        for k in words:
            if k[-2:] == '에서' and k[0] not in [str(i) for i in range(10)]:
                data['where_ver2'][i] = k
                
# data 
with open('data_where_ver2.pkl', 'wb') as file:
    pickle.dump(data, file)
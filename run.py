# Preprocessing
import re
# one sentence
from konlpy.tag import Okt
from sklearn.feature_extraction.text import HashingVectorizer
from sklearn.metrics.pairwise import linear_kernel
# slot rule(when, where)
from rule_loader import rule_loader
from rule_process import rule_process
import logging
import time
import pdb
from tqdm import tqdm

# input
print('제목을 입력하세요 : ')
title = input()
print('본문을 입력하세요 : ')
text = input()

# preprocessing
def cleansing(text):
    t = text.split('.')
    result = []
    for i in t:
        t1 = i.replace(u'\xa0',' ') #\xa0제거
        t2 = re.sub('([a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+)', '', string = t1) #email제거
        t3 = re.sub("\n","",t2) #\n제거
        t4 = re.sub('[^a-zA-Z가-힣0-9\.\% ]','',string = t3) #특수기호 제거
        t5 = t4.strip() #앞뒤문장 공백 제거
        t6 = " ".join(t5.split()) #문장 중간 중복된 공백 제거
        t7 = t6.replace(". ",".")
        t8 = re.split('(?<=[^0-9])[\.|\n]', t7) #.기준으로 문장 분리
        
        result.extend(t8)
    
    f_result = [r for r in result if len(r) > 1] # 비어있는 데이터 삭제
        
    return f_result
        
# tokenizer
def tokenizer(raw, pos=["Noun","Alpha","Verb","Number"], stopword=[], convertStrings=False):
    okt = Okt()
    return [
        word for word, tag in okt.pos(raw, norm=True, stem=True)
            if len(word) > 1 and tag in pos and word not in stopword
        ]

# one sentence summarize
def sentence_1(title, data):
    vectorize = HashingVectorizer(tokenizer=tokenizer, n_features=7)
    try:
        X = vectorize.fit_transform(data)
        srch_vector = vectorize.transform([title])
        cosine_similar = linear_kernel(srch_vector, X).flatten()
        sim_rank_idx = cosine_similar.argsort()[::-1]
    
        return sorted(zip(cosine_similar, data), reverse=True)[0]
    except:
        return None

# slot extraction    
def slot_extraction(text):
    # rule 
    rl = rule_loader(logger=None)
    rl.load('./rule/timex3.rule') #when
    rl.load('./rule/where.rule') #where
    
    if not rl.generate_rules():
        exit()
    
    rp = rule_process(rules = rl.get_rules(), logger = None)
    rp.indexing()
    use_indexing = True
    
    result, variables, matched = rp.process(text, indexing = use_indexing)
    result = rp.merge_slot(result, text, rl.get_policy())
      
    return result    

### 5H1W
# where
def where(text):  
    slot = slot_extraction(text)
    where = []
    for _, s in enumerate(slot):
        if s['name'] == 'slot_location':
            where.append(s['text'])
    
    if not where:
        words = text.split(" ")
        for w in words:
            if w[-2:] == '에서' and w[0] not in [str(i) for i in range(10)]:
                where.append(w)
    
    return where

# when
def when(text):
    slot = slot_extraction(text)
    when = []
    for _, s in enumerate(slot):
        if s['name'] == 'slot_timex3':
            when.append(s['text'])
            
    return when
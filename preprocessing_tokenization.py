import pandas as pd
import os
data = pd.read_csv('News2.csv')

data['text_token'] = None

from nltk.tokenize import LineTokenizer
from nltk.tokenize import SpaceTokenizer

for i in range(len(data)):
    try:
        mass = data['text'][i]
        line_tokenizer = LineTokenizer()
        sentences = line_tokenizer.tokenize(mass) #줄바꿈 문자 기준으로 나누기
        sentence = []
        for j in range(len(sentences)):
            space_tokenizer = SpaceTokenizer()
            sentence.append(space_tokenizer.tokenize(sentences[j])) #공백으로 나누기
        data['text_token'][i] = sentence
    except:
        pass

import pickle
with open('News_token.pkl', 'wb') as file:
    pickle.dump(data, file)
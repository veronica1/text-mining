import pickle
from sklearn.feature_extraction.text import HashingVectorizer
from konlpy.tag import Okt
from sklearn.metrics.pairwise import linear_kernel

# data load
with open('News_clean_final2.pkl','rb') as f:
	data = pickle.load(f)
    
Okt = Okt()

# tokenizer
def tokenizer(raw, pos=["Noun","Alpha","Verb","Number"], stopword=[], convertStrings=False):
    return [
        word for word, tag in Okt.pos(raw, norm=True, stem=True)
            if len(word) > 1 and tag in pos and word not in stopword
        ]

vectorize = HashingVectorizer(tokenizer=tokenizer, n_features=7)

# one sentence summarize
def sentence_1(title, data):
    try:
        X = vectorize.fit_transform(data)
        srch_vector = vectorize.transform(title)
        cosine_similar = linear_kernel(srch_vector, X).flatten()
        sim_rank_idx = cosine_similar.argsort()[::-1]
    
        return sorted(zip(cosine_similar, data), reverse=True)[0]
    except:
        return None

# summarize    
data['summarize'] = [sentence_1([data['title'][i]],data['Clean_News'][i]) for i in range(len(data))]

# summarize score
data['summarize_score'] = [data['summarize'][i][0] 
                           if data['summarize'][i] is not None else None 
                           for i in range(len(data))]

# summarize text
data['summarize_text'] = [data['summarize'][i][1] 
                          if data['summarize'][i] is not None else None 
                          for i in range(len(data))]

# don't need
data = data.drop(['summarize'], axis = 1)

# text space token
data['text_token'] = None

from nltk.tokenize import LineTokenizer
from nltk.tokenize import SpaceTokenizer

for i in range(len(data)):
    try:
        mass = data['summarize_text'][i]
        line_tokenizer = LineTokenizer()
        sentences = line_tokenizer.tokenize(mass) #줄바꿈 문자 기준으로 나누기
        sentence = []
        for j in range(len(sentences)):
            space_tokenizer = SpaceTokenizer()
            sentence.append(space_tokenizer.tokenize(sentences[j])) #공백으로 나누기
        data['text_token'][i] = sentence
    except:
        pass

# twitter tag
from konlpy.tag import Twitter

data['token'] = None

twitter = Twitter()
for i in range(len(data)):
    try:
        data['token_pos'][i] = twitter.pos(data['summarize_text'][0])
    except:
        pass
    
with open('News_summarize.pkl', 'wb') as file:
    pickle.dump(data, file)    
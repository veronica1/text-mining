import pickle

with open('data_when_where.pkl','rb') as f:
	data = pickle.load(f)
    
data['why_ver2'] = [[] for i in range(data.shape[0])]

# extraction why
for i in range(len(data)):
    sentence = [s for s in data['Clean_News'][i] if ' 위해 ' in s]
    for j, s in enumerate(sentence):
        words = s.split(" ")
        target = [words.index(w) for w in words if w == '위해']
        why = [words[w] for w in range(0, target[0]+1)]
        data['why_ver2'][i] = ' '.join(why)
        
with open('data_why_ver2.pkl', 'wb') as file:
    pickle.dump(data, file)
import requests
from bs4 import BeautifulSoup 
import pandas as pd
import os

#카테고리별 url
category = ['society','politics','economic','foreign','culture','digital']

url_all = []
for i in range(len(category)):
    cate = category[i]
    url = 'https://news.daum.net/breakingnews/' + cate + '?page={}'
    i += 1
    url_all.append(url)

    
#카테고리별 각 페이지에 있는 기사 url
headers = {'User-Agent' : 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36'}

link = []
for i in range(len(url_all)): #카테고리
    print("category : ", category[i])
    for j in range(1,11): #페이지수 
        url = url_all[i]
        print("page : ", j)
        res = requests.get(url.format(j),headers=headers)
        if res.status_code == 200:
            html = BeautifulSoup(res.text,'html.parser')
            all_a = html.select('div.cont_thumb strong a')
            all_a = all_a[0:15]
            all_link = [a.attrs['href'] for a in all_a]
        link.append([category[i],all_link])    

#df 저장
result = []
for i in range(len(link)):
    target = link[i]
    cate = target[0]
    url = target[1]
    for j in range(len(url)):
        result.append([cate,url[j]])
        
News = pd.DataFrame(result)
News.columns = ['category','URL']
News["title"] = ""
News["text"] = ""

#title
print("제목 크롤링 중...")
for i in range(len(News)):
    url = News["URL"][i]
    resp = requests.get(url)
    soup = BeautifulSoup(resp.text)
    news_title = soup.select_one('h3.tit_view')
    News['title'][i] = news_title.get_text() 
    print("title : " ,i)
print("제목 크롤링 완료")

#text
print("본문 크롤링 중...")
for i in range(len(News)):
    content = ''
    url = News["URL"][i]
    resp = requests.get(url)
    soup = BeautifulSoup(resp.text)
    content = '\n'.join([p.get_text() for p in soup.select('div#harmonyContainer p')])
    News['text'][i] = content  
    print('text : ',i)
print("본문 크롤링 완료")

path = os.getcwd()
News.to_csv(path+"/News.csv",header=True,encoding='utf-8-sig',index=False)

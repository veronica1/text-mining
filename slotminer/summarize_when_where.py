import pickle

with open('News_summarize.pkl','rb') as f:
	data = pickle.load(f)
    
data['when'] = [[] for i in range(data.shape[0])]
data['where'] = [[] for i in range(data.shape[0])]

from rule_loader import rule_loader
from rule_process import rule_process
import logging
import time
import pdb
from tqdm import tqdm

# 규칙을 파일로부터 읽어들임
rl = rule_loader(logger=None)
rl.load('./rule/timex3.rule') #when
rl.load('./rule/where.rule') #where

if not rl.generate_rules():
    exit()
    
# 읽어들인 규칙을 화면에 표시
#rl.print_rules()

# 규칙 '수행(실행)' 객체 생성
rp = rule_process(rules=rl.get_rules(), logger=None)

# 규칙에 대한 indexing
rp.indexing()
use_indexing = True

# slot 
slot = []
start_time = time.time()

for i in range(len(data)):
    
    text = data['summarize_text'][i]

    if not text:
        slot.append([])
        continue
    
    result, variables, matched = rp.process(text, indexing=use_indexing)
    result = rp.merge_slot(result, text, rl.get_policy())

    slot.append(result)
    
end_time = time.time()    
#print('수행시간={}초'.format(end_time - start_time))

# extract when, where
for i, s in enumerate(slot):
    for t in range(len(s)):
        st = s[t]
        if st['name'] == 'slot_location':  
            data['where'][i].append(st['text'])
        if st['name'] == 'slot_timex3':
            data['when'][i].append(st['text'])

# data 
with open('data_when_where.pkl', 'wb') as file:
    pickle.dump(data, file)
import pickle

with open('News_summarize.pkl','rb') as f:
	data = pickle.load(f)
    
########## how extraction ver1 ##########
data['how_ver1'] = [[] for i in range(data.shape[0])]

for i in range(len(data)):
    sentence = [s for s in data['Clean_News'][i] if ' 위해 ' in s]
    for j, s in enumerate(sentence):
        words = s.split(" ")
        target = [words.index(w) for w in words if w == '위해']
        how = [words[w] for w in range(target[0]+1, len(words))]
        data['how_ver1'][i] = ' '.join(how)
        
########## how extraction ver2 ##########
data['how_ver2'] = [[] for i in range(data.shape[0])]

# kkma tag
from konlpy.tag import *
kkma = Kkma()

def token(data):
    try:
        return kkma.pos(data)
    except:
        return None

# find Josa
def josa(token):
    try:
        idx = []
        for i, t in enumerate(token):
            if t[1][0] == 'J' or t[1][0] == 'E':
                idx.append([i, t])
        return idx
    except:
        None

print('how extraction start')

# how extraction
for i in range(len(data)):
    data_token = token(data['summarize_text'][i])
    data_josa = josa(data_token)
    for j in range(len(data_josa)):
        pre = data_josa[j-1]
        now = data_josa[j]
        how = []
        if now[1][1][0] == 'E':
            for k in range(pre[0]+1, now[0]+1):
                how.append(data_token[k][0])
        if how:
            data['how_ver2'][i].append(''.join(how))
            
print('done')

with open('data_how.pkl', 'wb') as file:
    pickle.dump(data, file)
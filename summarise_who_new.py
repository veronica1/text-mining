import  pickle 
with open('News_summarize.pkl','rb') as f:
    data = pickle.load(f)

data['who'] = [[] for i in range(data.shape[0])]

# who extraction
for i in range(len(data)):
    try:
        text = data['summarize_text'][i]
        words = text.split(" ")
        who = []
        for w in words:
            if w[-1:] in ['은', '는', '이', '가']:
                who.append(w)        
        data['who'][i] = who
    except:
        None

raw = data['who'][0]

for sentence in raw:
    pos_tag = kkma.pos(sentence)
    print(pos_tag)

idx = []
for i, t in enumerate(pos_tag):
    if t[1] == "NNG" :
        idx.append([t,i])
        
print('idx: ', idx)

who_new = []
for i in range(len(idx)):
    who_new.append(idx[i][0])
print('who_new:', who_new)

import pickle

with open('News_summarize.pkl','rb') as f:
	data = pickle.load(f)
    
data['what'] = [[] for i in range(data.shape[0])]

# kkma tag
from konlpy.tag import *
kkma = Kkma()

def token(data):
    try:
        return kkma.pos(data)
    except:
        return None

# find Josa
def josa(token):
    try:
        idx = []
        for i, t in enumerate(token):
            if t[1][0] == 'J':
                idx.append([t, i])
        return idx
    except:
        None

print('start what extraction')

# what extraction
for i in range(len(data)):
    data_token = token(data['summarize_text'][i])
    data_josa = josa(data_token)
    for j in range(len(data_josa)):
        pre = data_josa[j-1]
        now = data_josa[j]
        what = []
        if now[0][1] == 'JKO':
            for k in range(pre[1]+1, now[1]+1):
                what.append(data_token[k][0])
        if what:
            data['what'][i].append(''.join(what))
            
print('done')

with open('data_what.pkl', 'wb') as file:
    pickle.dump(data, file)
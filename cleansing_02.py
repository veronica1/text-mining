import pandas as pd
News = pd.read_csv('./News2.csv',sep=',')

#text가져오기
text = News.text

#text변수 길이 : 900
#len(text)

#뉴스 본문 전처리
result = []

import re

for i in range(len(text)):
	t = text[i]
	t = str(t).replace(u'\xa0',' ') #\xa0 제거
	t = re.sub('([a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+)', '', string = t) #email제거
	t = re.sub('\n','',t) #\n제거
	s = re.sub('[^a-zA-Z가-힣0-9\.\% ]','',string = t) #특수기호제거
	a = s.strip() #앞뒤문장 공백 제거
	b = " ".join(a.split()) #문장 중간 중복된 공백 제거
	b = b.replace(". ",".")
	c = re.split('(?<=[^0-9])[\.|\n]',b) #.기준으로 문장분리
	
	i += 1
	result.append(c)

#비어있는 데이터 삭제
f_result = []

for i in range(len(result)):
	news_result = result[i]
	news_result = [news_result[j] for j in range(len(news_result)) if len(news_result[j])>1]
	i += 1
	f_result.append(news_result)

#최종결과 : f_result

#전처리 후 뉴스 본문 데이터 열 추가
News['Clean_News'] = f_result

#pickle로 저장
import pickle
with open("News_clean_final.pkl","wb") as file:
	pickle.dump(News, file)


#pickle로 불러오기
import pickle
with open("News_clean_final.pkl","rb") as f:
	data = pickle.load(f)
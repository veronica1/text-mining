import pickle

with open('data_when_where.pkl','rb') as f:
	data = pickle.load(f)
        
data['who_ver2'] = [[] for i in range(data.shape[0])]

# who extraction
for i in range(len(data)):
    try:
        text = data['summarize_text'][i]
        words = text.split(" ")
        who = []
        for w in words:
            if w[-1:] in ['은', '는', '이', '가']:
                who.append(w)        
        data['who_ver2'][i] = who
    except:
        None
        
with open('data_who_ver2.pkl', 'wb') as file:
    pickle.dump(data, file)